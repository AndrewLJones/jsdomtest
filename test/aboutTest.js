var assert = require('assert');

suite('About controller', function() {

  test('validations', function(done) {
    this.timeout(5000);

    var jsdom = require("jsdom");
    jsdom.env({
      html: '<html ng-app="app"><body><div ng-controller="Controller"><div id="about-test">{{firstName}}</div></div></body></html>',
      scripts: [
        __dirname + '/../node_modules/angular/angular.min.js',
        __dirname + '/../node_modules/angular-route/angular-route.min.js'
      ],
      features: {
        FetchExternalResources: ["script"],
        ProcessExternalResources: ["script"],
      },
      done: function(errors, window) {
        if(errors != null) console.log('Errors', errors);

        var $ = function(selector) {
          return window.document.querySelector(selector);
        }

        var trigger = function(el, ev) {
          var e = window.document.createEvent('UIEvents');
          e.initEvent(ev, true, true);
          el.dispatchEvent(e);
        }

        var Controller = function($scope) {

          $scope.firstName = 'andrew';

          var runTests = function() {

            var about_div = $('#about-test');
            assert.equal(about_div.innerHTML, 'andrew');

            done();

          };
          setTimeout(runTests, 1000);
        }

        window
          .angular
          .module('app', [])
          .controller('Controller', Controller);

      }
    });

  });

});